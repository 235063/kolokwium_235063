import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JPanel;

public class KoloPanel extends JPanel {


        private Point punkt;

        public KoloPanel() {
            int width = 400;
            this.setPreferredSize(new Dimension(width, width));
            this.punkt = new Point(width / 2, width / 2);
        }

        public void addPoint(int x, int y) {
            punkt = new Point(x, y);
        }

        public Point OstatniPunkt() {
            return punkt;
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);

            g.setColor(Color.BLACK);
            g.fillOval(punkt.x - 20, punkt.y - 20, 40, 40);
        }
    }