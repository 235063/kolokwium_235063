import java.awt.Point;
import javax.swing.InputMap;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

public class Kolokwium implements Runnable {

    private JFrame ramka;

    @Override
    public void run() {
        ramka = new JFrame("Poruszanie okręgiem klawiatura");
        ramka.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        KoloPanel panel = new KoloPanel();
        setKeyBindings(panel);
        ramka.add(panel);

        ramka.pack();
        ramka.setVisible(true);
    }

    private void setKeyBindings(KoloPanel circlePanel) {
        InputMap inputMap = circlePanel.getInputMap(JPanel.WHEN_IN_FOCUSED_WINDOW);

        inputMap.put(KeyStroke.getKeyStroke("UP"), "gora");
        inputMap.put(KeyStroke.getKeyStroke("DOWN"), "dol");
        inputMap.put(KeyStroke.getKeyStroke("LEFT"), "lewo");
        inputMap.put(KeyStroke.getKeyStroke("RIGHT"), "prawo");

        circlePanel.getActionMap().put("gora",
                new Ruch(circlePanel, new Point(0, -10)));
        circlePanel.getActionMap().put("dol",
                new Ruch(circlePanel, new Point(0, 10)));
        circlePanel.getActionMap().put("lewo",
                new Ruch(circlePanel, new Point(-10, 0)));
        circlePanel.getActionMap().put("prawo",
                new Ruch(circlePanel, new Point(10, 0)));
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Kolokwium());
    }

    

    

}