import java.awt.Point;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

public class Ruch extends AbstractAction {


        private KoloPanel panelkolo;

        private Point movePoint;

        public Ruch(KoloPanel kolopanel, Point ruch) {
            this.panelkolo = kolopanel;
            this.movePoint = ruch;
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            Point p = panelkolo.OstatniPunkt();
            p.x += movePoint.x;
            p.y += movePoint.y;
            panelkolo.addPoint(p.x, p.y);
            panelkolo.repaint();
        }

    }